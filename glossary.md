Since Quill.js doesn't seem to document its strange jargon-y terms anywhere, here's a glossary that I've put together for it. No guarantees that it's correct! But I've done my best.

__Quill__ - The WYSIWYG editor library

__Parchment__ - The internal model used in Quill to implement the document tree

__Scroll__ - A document, expressed as a tree, technically also a Blot (node) itself, specifically the root node

__Blot__ - A node in the document tree

__Block (Blot)__ - A block-level node

__Inline (Blot)__ - An inline (formatting) node

__Text (Blot)__ - A node that contains only(!) raw text contents

__Break (Blot)__ - A node that contains nothing, used as a placeholder where there is no actual content

__"a format"__ - A specific formatting attribute (width, height, is bold, ...)

__`.format(...)`__ - The API method that is used to set a formatting attribute on some selection